#ifndef LIST_H
#define LIST_H

#include "item.h"

template<typename T>
class List{
    Item<T> * first;
    Item<T> * last;
    unsigned int itemCount;
    void initList();
public:
    List();
    ~List();
    unsigned int getItemCount() const{return itemCount;}
    bool isEmpty() const{
        return(itemCount == 0);
    }
    void pushBack(const T d);
    void pushFront(const T d);
    void print();
    void popBack();
    void popFront();
    void clean();
    void sort();
    void swap(Item<T> * a, Item<T> *b);
    Item<T> * FindMinFrom(Item<T> * p);
};

template<typename T>
List<T>::List()
 : first(new Item<T>()), last(new Item<T>()), itemCount(0)
{
    initList();
}

template<typename T>
void List<T>::initList()
{
    first->next = last;
    last->prev = first;

}

template<typename T>
void List<T>::pushBack(const T d)
{
    Item<T> * a = new Item<T>();
    last->next = a;
    a->prev = last;
    last->data = d;
    last = a;
    itemCount++;
}

template<typename T>
void List<T>::pushFront(const T d)
{
    Item<T> * a = new Item<T>();
    first->prev = a;
    a->next = first;
    first->data = d;
    first = a;
    itemCount++;
}

template<typename T>
void List<T>::print()
{
    Item<T> * temp = first->next;
    while(temp != last){
        std::cout << temp->data << std::endl;
        temp = temp->next;
    }
}

template<typename T>
void List<T>::popBack()
{
    if(!isEmpty()){
        last = last->prev;
        delete last->next;
        last->next = nullptr;
        itemCount--;
    }
}

template<typename T>
void List<T>::popFront(){
    if(!isEmpty()){
        first = first->next;
        delete first->prev;
        first->prev = nullptr;
        itemCount--;
    }
}

template<typename T>
void List<T>::clean()
{
    while(!isEmpty()){
        popFront();
    }
}

template<typename T>
List<T>::~List()
{
    clean();
    delete first;
    delete last;
}

template<typename T>
void List<T>::sort()
{
    Item<T> * temp = first;
    while(temp != last){
        swap(temp, FindMinFrom(temp));
        temp = temp->next;
    }
}

template<typename T>
Item<T> * List<T>::FindMinFrom(Item<T> * p)
{
    Item<T> * min = p->next;
    Item<T> * temp = p->next;
    while(temp != last){
        if(temp->data < min->data){
            min = temp;
        }
        temp = temp->next;
    }
    return min;
}

template<typename T>
void List<T>::swap(Item<T> * a, Item<T> *b)
{
    T x = a->data;
    a->data = b->data;
    b->data = x;
}

#endif // LIST_H
