#include <iostream>
#include "list.h"
using namespace std;

int main()
{
    List<int> l;
    List<double> ldouble;

    for(int i = 0; i < 10; i++){
        l.pushBack(i);
        l.pushFront(20-i);
    }
    l.print();
    l.popBack();
    l.popFront();
    cout << endl;
    l.print();
    cout << endl << "sorted" << endl;
    l.sort();
    l.print();

    return 0;
}
