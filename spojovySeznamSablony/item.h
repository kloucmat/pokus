#ifndef ITEM_H
#define ITEM_H

#include <iostream>

template<typename T>
class Item{
    T data;
    Item<T> *prev;
    Item<T> *next;
public:
    Item() : prev(nullptr), next(nullptr){}
    Item(T d) : data(d), prev(nullptr), next(nullptr){}
    void setData(const T d){data = d;}
    T getData() const{return data;}
    void print() const{
        std::cout << data;
    }
    void printLine() const{
        print();
        std::cout << std::endl;
    }
    template<typename X> friend class List;
};

#endif // ITEM_H
